package uz.pdp.ikki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IkkiApplication {

    public static void main(String[] args) {
        SpringApplication.run(IkkiApplication.class, args);
    }

}
